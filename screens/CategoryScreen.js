import React from 'react';
import { View,
  FlatList,
  TouchableOpacity,
  TextInput,
  Text,
  StyleSheet
} from 'react-native';
import { Icon } from 'expo';
import Data from '../utils/Data';
import ProfileCard from '../components/ProfileCard';
import CategoryPicker from '../components/CategoryPicker';

export default class CategoryScreen extends React.Component {
  static navigationOptions = {
    title: 'Categories'
};

constructor(props) {
    super(props);
    this.state = {
        searchValue:'',
        searchResult:[],
    };
}

render() {
  return (
      <View style={ styles.container }>
          <View style={ styles.searchBar }>
              <CategoryPicker 
                selectedValue = {this.state.searchValue}
                onValueChange = {(itemValue) => this.setState({searchValue:itemValue})}
              />
              <TouchableOpacity onPress={ this.searchByCategory }>
                  <Icon.FontAwesome
                      name={ 'search' }
                      size={ 30 }
                      color={ 'black' }
                  />
              </TouchableOpacity>
          </View>
          <FlatList
              data={ this.state.searchResult }
              keyExtractor={(item, index) => index.toString()} 
              renderItem={ ({item}, index) => <ProfileCard profile={item.profile} userId={index} /> }
          />
      </View>
  );
}

searchByCategory = async () => {
  let searchResult = this.state.searchValue && await Data.searchProfileByCategory(this.state.searchValue);
  this.setState({searchResult});
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  padding: 15
},
searchBar: {
  flexDirection: 'row',
},
inputText: {
  flex: 1,
  backgroundColor: '#fff',
  borderRadius: 5,
  fontSize: 18
}
})