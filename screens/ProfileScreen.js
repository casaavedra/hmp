import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button
} from 'react-native';
import { AppLoading, Icon } from 'expo';
import Data from '../utils/Data';
import Profile from "../components/Profile";


export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Profile',
    headerRight: (
        <Button
          onPress={() => this.logout}
          title='Logout'
          color="gray"
        />
      ),
  };

  constructor(props) {
      super(props);
      const userId = this.props.navigation.getParam('userId');
      this.state = {
          userId: userId,
          isLoading: true,
          currentUser: {}
      };
      this.getCurrentUser(userId);
      this.logout = this.logout.bind(this);
  }

  logout = () => {
    this.props.navigation.navigate('Login');
  }

  getCurrentUser = async (userId) => {
        let currentUser = await Data.getCurrentUser(userId);
        this.setState({currentUser, isLoading:false});
  };

  render() {
      if (this.state.isLoading) {
          return (
              <AppLoading />
          );
      } else {
          return (
              <Profile 
                profile={this.state.currentUser.profile} 
                userId={ this.state.userId } 
                isModal={ false }/>
          );
      }
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 20
    },
    profileImage:{
        flex:1,
    },
    profileText:{
        flex: 8,
    }
});
