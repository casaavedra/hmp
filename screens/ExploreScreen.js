import React, { Component } from 'react';
import { View,
    FlatList,
    TouchableOpacity,
    TextInput,
    Text,
    StyleSheet
} from 'react-native';
import { Icon } from 'expo';
import Data from '../utils/Data';
import ProfileCard from '../components/ProfileCard';

export default class ExploreScreen extends Component {
    static navigationOptions = {
        title: 'Explore'
    };

    constructor(props) {
        super(props);
        this.state = {
            searchText:'',
            searchResult:[],
        };
    }

    render() {
        return (
            <View style={ styles.container }>
                <View style={ styles.searchBar }>
                    <TextInput
                        style={ styles.inputText }
                        placeholder={ 'Search' }
                        onChangeText={ (searchText) => this.setState({searchText}) }
                    />
                    <TouchableOpacity onPress={ this.searchByName }>
                        <Icon.FontAwesome
                            name={ 'search' }
                            size={ 30 }
                            color={ 'black' }
                        />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={ this.state.searchResult }
                    keyExtractor={(item, index) => index.toString()} 
                    renderItem={ ({item}, index) => <ProfileCard profile={item.profile} userId={index} /> }
                />
            </View>
        );
    }

    searchByName = async () => {
        let searchResult = this.state.searchText && await Data.searchProfileByName(this.state.searchText);
        this.setState({searchResult});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15
    },
    searchBar: {
        flexDirection: 'row',
    },
    inputText: {
        flex: 1,
        backgroundColor: '#fff',
        borderRadius: 5,
        fontSize: 18
    }
})