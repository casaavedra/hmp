import React from 'react';
import {
  TextInput,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
} from 'react-native';
import Data from '../utils/Data';

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:''
        }
    }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <TextInput
                    style={styles.textInput}
                    placeholder="Username"
                    onChangeText={(username) => this.setState({username})}
                    />
                <TextInput
                    style={styles.textInput}
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({password})}
                    />
                <TouchableOpacity style={styles.loginButton} onPress={this._handleLogin}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.linkText} onPress={this.goToRegister}>
                    <Text style={styles.buttonText}>Signup</Text>
                </TouchableOpacity> 
            </KeyboardAvoidingView>
        );
    }

    _handleLogin = async () => {
        try {
            let users = await Data.getUsers();
            users = users.map((user,index) => {
                if (user.username === this.state.username && user.password === this.state.password) {
                    this.props.navigation.navigate('Profile', { userId: index });
                }
            });
        } catch (error) {
            console.error(error);
        }
    }

    goToRegister = () => {
        this.props.navigation.navigate('Register');
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    textInput:{
        margin:10,
        borderBottomWidth:1,
        width:250,
        fontSize:24
    },
    loginButton:{
        alignItems:'center',
        justifyContent:'center',
        marginTop:20,
        borderRadius:10,
        width:125,
        height:50,
        backgroundColor:'skyblue'
    },
    buttonText:{
        fontSize:20
    },
    linkText: {
        marginTop:20
    }
});
