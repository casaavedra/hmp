import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet,
    AsyncStorage
} from 'react-native';
import Data from '../utils/Data';

export default class RegisterScreen extends React.Component {
    static navigationOptions = {
        header:null
    };

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:'',
            error:''
        }
    }

    render() {
        let error;
        if (this.state.error)
            error = <Text style={styles.errorMessage}>Username already taken, please try again.</Text>;

        return(
            <View style={styles.container}>
                <Text style={styles.title}>Register</Text>
                {error}
                <View>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Username'}
                        onChangeText={(username) => { this.setState({username, error:''}); }}
                    />
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        onChangeText={(password) => { this.setState({password}) }}
                    />
                </View>
                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this._handleSignup}
                    >
                        <Text style={styles.buttonText}>Signup</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.goToLogin}
                    >
                        <Text style={styles.buttonText}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    _handleSignup = async () => {
        try {
            let username = this.state.username;
            let password = this.state.password;
            let users = await Data.getUsers();

            users = users.map((user) => {
                if (user.username === username)
                    this.setState({error: "Username taken"});
                return user;
            });

            if (!this.state.error && username && password) {
                this.createUser(username, password);
            }
        } catch (error) {
            console.log(error);
        }
    }

    createUser = async (username, password) => {
        try {
            await Data.createUser(username, password);
            this.setState({error:''});
            this.goToLogin();
        } catch (error) {
            console.log(error);
        }
    }

    goToLogin = () => {
        this.props.navigation.navigate('Login');
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        alignContent: 'center',
        fontSize: 24
    },
    textInput: {
        margin:10,
        borderBottomWidth:1,
        width:250,
        fontSize:20
    },
    button: {
        alignItems:'center',
        justifyContent:'center',
        marginTop:20,
        borderRadius:10,
        width:125,
        height:50,
        backgroundColor:'skyblue'
    },
    buttonText: {
        fontSize:18
    },
    errorMessage: {
        alignContent:"center",
        fontSize: 20,
        color: 'red'
    }
});
