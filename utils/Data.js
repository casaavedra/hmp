import { AsyncStorage } from 'react-native';
import UserSchema from '../utils/UserSchema';

export default class Data {
    static getUsers = async () => {
        let data = await AsyncStorage.getItem('data');
        data = JSON.parse(data);
        if (!data){
            data = {users:fakeUsers};
            await AsyncStorage.setItem('data',JSON.stringify(data));
        }

        let users = data.users;
        return users;
    };

    static getCurrentUser = async (userId) => {
        let users = await Data.getUsers();
        return users[userId];
    };

    static saveUserProfile = async (userId, profile) => {
        let users = await Data.getUsers();
        users[userId].profile = profile;
        await AsyncStorage.mergeItem('data',JSON.stringify({users}));
    }

    static createUser = async (username, password) => {
        let users = await Data.getUsers();
        let user = new UserSchema(username, password);
        users.push(user.toJSON());
        await AsyncStorage.mergeItem('data',JSON.stringify({users}));
    }

    static searchProfileByName = async (searchText) => {
        let users = await Data.getUsers();
        return users.filter( user => {
            const searchName = searchText.toLowerCase();
            const name = user.profile.name.toLowerCase();
            return name.includes(searchName);
        })
    }

    static searchProfileByCategory = async (searchValue) => {
        let users = await Data.getUsers();
        return users.filter( user => {
            const searchCategory = searchValue.toLowerCase();
            const category = user.profile.service.toLowerCase();
            return category.includes(searchCategory);
        })
    }
}

const fakeUsers = [
    {
        username:'',
        password:'',
        profile: {
            name:'Jorge Rosal',
            service:'',
            description:'',
            address:'',
            phone:0,
            email:'',
            rate:[]
        }
    },
    {
        username:'',
        password:'',
        profile: {
            name:'Karina Ceballos',
            service:'',
            description:'',
            address:'',
            phone:0,
            email:'',
            rate:[]
        }
    },
    {
        username:'',
        password:'',
        profile: {
            name:'Luis Villalobos',
            service:'',
            description:'',
            address:'',
            phone:0,
            email:'',
            rate:[]
        }
    },
    {
        username:'',
        password:'',
        profile: {
            name:'Enrique Olvera',
            service:'',
            description:'',
            address:'',
            phone:0,
            email:'',
            rate:[]
        }
    },
    {
        username:'',
        password:'',
        profile: {
            name:'Yolanda Garcia',
            service:'',
            description:'',
            address:'',
            phone:0,
            email:'',
            rate:[]
        }
    },
]