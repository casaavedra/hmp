export default class UserSchema {
    username;
    password;
    profile = {
        name:'',
        service:'',
        description:'',
        address:'',
        phone:0,
        email:'',
        rate:[]
    };

    constructor(username, password) {
        this.username = username;
        this.password = password;
    }

    toJSON(){
        return {
            username:this.username,
            password:this.password,
            profile:this.profile
        }
    }
};
