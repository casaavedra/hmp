import React, { Component } from 'react';
import {
    View,
    Picker,
} from 'react-native';

export default class CategoryPicker extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <View style={{borderBottomWidth:1, marginBottom:10, flex:1}}>
                <Picker
                    style={{height:20}}
                    selectedValue={ this.props.selectedValue }
                    onValueChange={ this.props.onValueChange }
                >
                    {categoriesList.map( item => (
                        <Picker.Item key={ item } label={ item } value={ item }/>
                    ))}
                </Picker>
            </View>
        );
    }
}

const categoriesList = [
    'Accountant',
    'Florist',
    'Veterinarian',
    'Instructor',
    'Web developer',
]