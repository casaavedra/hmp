import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Modal,
    StyleSheet
} from 'react-native';
import Profile from './Profile';

export default class ProfileCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
        }
    }

    render() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.showModal}
                    onRequestClose={() => {this.setState({showModal:false})}}
                >
                    <Profile 
                        profile={ this.props.profile }
                        userId={ this.props.userId }
                        isModal={ true }
                    />
                </Modal>
                <TouchableOpacity 
                    style={ styles.container }
                    onPress={ this.openModal }
                >
                    <Text>{this.props.profile.name}</Text>
                    <Text>{this.props.profile.service}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    openModal = () => {
        this.setState({showModal:true});
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        elevation: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height:50, 
        borderWidth:1
    }
})