import React from 'react';
import {
    Image, 
    ScrollView, 
    Text, 
    TouchableOpacity, 
    View, 
    TextInput, 
    StyleSheet,
    KeyboardAvoidingView 
} from "react-native";
import {Icon} from "expo";
import Data from '../utils/Data';
import CategoryPicker from '../components/CategoryPicker';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: this.props.userId,
            profile: this.props.profile,
            isEditable: false,
            isModal: this.props.isModal,
        };
    }

    render() {
        if (!this.state.isEditable) {
            let fieldsArray = this.getFieldsArray();
            let valuesArray = this.getValuesArray(fieldsArray);
            let editButton;
            if (!this.state.isModal) {
                editButton = <View style={ styles.editButton }>
                    <TouchableOpacity
                        onPress={() => this.setState({isEditable: true})}
                    >
                        <Icon.FontAwesome
                            name={'edit'}
                            size={30}
                            color={'#777'}
                        />
                    </TouchableOpacity>
                </View>
            }
            return (
                <View>
                    <ScrollView contentContainerStyle={ styles.container }>
                        { editButton }
                        <Image
                            style={styles.profileImage}
                            source={require('../assets/images/profile_avatar_placeholder.png')}
                        />
                        <View style={styles.profile}>
                            <View style={styles.profileFields}>
                                {fieldsArray.map(field => (
                                    <Text
                                        style={styles.profileText}
                                        key={field}
                                    >
                                        {field}
                                    </Text> )
                                )}
                            </View>
                            <View style={styles.profileValues}>
                                {valuesArray.map(value => (
                                    <Text
                                        style={styles.profileText}
                                        key={value}
                                    >
                                        {value}
                                    </Text> )
                                )}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        } else {
            let fieldsArray = Object.keys(this.state.profile).filter(field => { return field !== 'rate' });
            let renderValue = (field) => {
                if (field !== 'service'){
                    return <TextInput
                        style={[styles.profileInputText, styles.profileText]}
                        key={field}
                        onChangeText={(text) => {this._handleFieldChange(text, field)}}
                    >
                        { this.state.profile[field] }
                    </TextInput>
                } else {
                    return <CategoryPicker 
                        selectedValue={ this.state.profile[field] }
                        onValueChange={ (itemValue) => { this._handleFieldChange(itemValue, field) } }
                    />
                }
            }
            return (
                <KeyboardAvoidingView behavior="padding" enabled>
                    <ScrollView contentContainerStyle={ styles.container }>
                        <Image
                            style={styles.profileImage}
                            source={require('../assets/images/profile_avatar_placeholder.png')}
                        />
                        <View style={styles.profile}>
                            <View style={styles.profileFields}>
                                {fieldsArray.map(field => (
                                    <Text
                                        style={styles.profileText}
                                        key={field}
                                    >
                                        {field}
                                    </Text> )
                                )}
                            </View>
                            <View style={styles.profileValues}>
                                {fieldsArray.map(field => { return renderValue(field) })}
                            </View>
                        </View>
                        <View style={ styles.buttonContainer }>
                            <TouchableOpacity 
                                style={ styles.button }
                                onPress={ this.saveProfile }
                                >
                                <Text style={ styles.buttonText }>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            );
        }
    }

    editProfile = () => {
        this.setState({isEditable: true});
    };

    getFieldsArray = () => {
        let fields = [];
        Object.keys(this.state.profile).forEach(key => {
            let value = this.state.profile[key];
            if (value)
                fields.push(key);
        });
        return fields;
    };

    getValuesArray = (fieldsArray) => {
        return fieldsArray.map(field => {
            let value = this.state.profile[field];
            if (Array.isArray(value)) {
                if (value.length) {
                    return value.reduce((sum,val) => sum + val) / value.length;
                }
                return 0;
            }
            return value;
        });
    };

    _handleFieldChange = (value, field) => {
        let profile = this.state.profile;
        profile[field] = value
        this.setState({profile});
    };

    saveProfile = async () => {
        await Data.saveUserProfile(this.state.userId, this.state.profile);
        this.setState({isEditable: false});
    }
    
}

const styles = StyleSheet.create({
    profile:{
        flex: 1,
        flexDirection: 'row',
        padding: 20,
    },
    profileImage:{
        flex:1,
    },
    profileFields:{
        flex: 4,
    },
    profileValues: {
        flex: 8,
    },
    profileText: {
        fontSize: 20,
        marginBottom:10,
    },
    profileInputText: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
    },
    button:{
        alignItems:'center',
        justifyContent:'center',
        marginTop:20,
        borderRadius:10,
        width:125,
        height:50,
        backgroundColor:'skyblue'
    },
    buttonContainer: {
        flex:1,
        alignItems:'center',
    },
    buttonText: {
        fontSize:18
    },
    container: {
        // flex: 1,
        alignItems: 'center',
        marginTop: 25,
        paddingHorizontal: 10
    },
    editButton: {
        position: 'absolute',
        right: 15,
        zIndex: 5,
    }
});
